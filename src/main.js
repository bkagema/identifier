import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './Vuex/Store'
import Vuetify from 'vuetify'
import Vuex from 'vuex'
import Axios from 'axios'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'babel-polyfill'
import moment from 'moment'
import VueMomentJS from 'vue-momentjs'

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(Axios)
Vue.use(VueMomentJS, moment)

Vue.use(Vuetify, {
  theme: {
    primary: '#14953e', // #E53935
    secondary: '#aa754a', // #FFCDD2
    accent: '' // #3F51B5
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
