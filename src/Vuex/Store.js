import Vue from 'vue'
import Vuex from 'vuex'
import ImageUploadStore from './ImageUpload/ImageUpload/store'


Vue.config.productionTip = false
Vue.use(Vuex)
Vue.config.debug = true

const debug = process.env.NODE_ENV !== 'production'

export const strict = false
export default new Vuex.Store({
  modules: {
    ImageUploadStore
  },
  strict: debug
})
