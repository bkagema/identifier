import Vue from 'vue'
import {UploadUrls} from '../Urls'
import router from '../../../router'
import VueNotifications from 'vue-notifications'

export default {
  get_all_uploads (context) {
    Vue.http.get(UploadUrls.getAllUploads).then(function (response) {
      context.commit('GET_ALL_UPLOADS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_uploads (context) {
    Vue.http.get(UploadUrls.getUploads).then(function (response) {
      context.commit('GET_UPLOADSS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_deleted_uploads (context) {
    Vue.http.get(UploadUrls.getDeletedUpload).then(function (response) {
      context.commit('GET_DELETED_UPLOADSS', response.data)
      context.dispatch('loading_false')
    })
  },
  get_upload (context, publicId) {
    Vue.http.get(UploadUrls.getUpload + publicId).then(function (response) {
      context.commit('GET_UPLOAD', response.data)
      context.dispatch('loading_false')
    })
  },
  post_upload (context, data) {
    Vue.http.post(UploadUrls.postUpload, data).then(function (response) {
      context.dispatch('get_all_uploads')
      router.push({
        name: data.redirect_url
      })
      VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    }).catch(function (error) {
      context.commit('UPDATE_ERRORS', error.data)
      context.dispatch('loading_false')
      VueNotifications.error({message: error.data.message})
      console.log(error.data)
    })
  },
  update_upload (context, data) {
    Vue.patch(UploadUrls.editUpload, data).then(function (response) {
      context.dispatch('get_all_uploads')
      router.push({
        name: data.redirect_url
      })
      context.dispatch('loading_false')
    })
  },
  delete_upload (context, data) {
    Vue.patch(UploadUrls.deleteUpload, data).then(function (response) {
      alert(response.data.message)
      context.commit('GET_DELETED_UPLOADS', response.data)
      router.push({
        name: 'Dashboard'
      })
      context.dispatch('loading_false')
    })
  },
  restore_upload (context, publicId) {
    Vue.http.get(UploadUrls.restoreUpload + publicId).then(function (response) {
      alert(response.data.message)
      context.commit('GET_ALL_UPLOADS', response.data)
      router.push({
        name: 'Module.Uploads'
      })
      // VueNotifications.success({message: response.data.message})
      context.dispatch('loading_false')
    })
  }
}
