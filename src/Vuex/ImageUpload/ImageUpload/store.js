import mutations from './mutations'
import actions from './actions'

const state = {
  all_uploads: [],
  uploads: [],
  uplaod: [],
  upload_butchery: [],
  upload_meat_type: [],
  deleted_uploads: []
}

export default {
  state, mutations, actions
}
