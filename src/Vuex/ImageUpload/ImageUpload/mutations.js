export default {
  GET_UPLOADS (state, data) {
    state.uploads = data.data
  },
  GET_ALL_UPLOADS (state, data) {
    state.all_uploads = data.data
  },
  GET_UPLOAD (state, data) {
    state.uplaod = data.data[0]
  },
  GET_DELETED_UPLOADS (state, data) {
    state.deleted_uploads = data.data
  }
}
