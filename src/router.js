import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'
// import Login from './views/Login.vue'
// Management
import Management from './views/Management.vue'
import Dashboard from './views/Management/Dashboard'

// ImageUpload
import ImageUpload from './views/Management/ImageUpload/ImageUpload'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    // {
    //   path: '/',
    //   component: Home,
    //   children: [
    //     {
    //       path: 'login',
    //       name: 'Login',
    //       component: Login
    //     }
    //   ]
    // },
    {
      path: '/',
      component: Management,
      children: [
        {
          path: '',
          name: 'Dashboard',
          component: Dashboard
        },
        // ImageUpload
        {
          path: 'ImageUpload',
          name: 'ImageUpload',
          component: ImageUpload
        }
        // {
        //   path: 'farmers',
        //   name: 'Farmers',
        //   component: Farmers
        // },
        // {
        //   path: 'single-farmer/:farmer_id',
        //   name: 'SingleFarmer',
        //   component: SingleFarmer
        // },
        // {
        //   path: 'farmer-settings',
        //   name: 'FarmerSettings',
        //   component: FarmerSettings
        // },
        // // Research
        // {
        //   path: 'AddNewResearch',
        //   name: 'AddNewResearch',
        //   component: AddNewResearch
        // },
        // {
        //   path: 'AllResearches',
        //   name: 'AllResearches',
        //   component: AllResearches
        // },
        // {
        //   path: 'SingleResearch/:research_id',
        //   name: 'SingleResearch',
        //   component: SingleResearch
        // },
        // {
        //   path: 'ResearchSettings',
        //   name: 'ResearchSettings',
        //   component: ResearchSettings
        // },
        // // Profile
        // {
        //   path: 'NewProfile',
        //   name: 'NewProfile',
        //   component: NewProfile
        // }
      ]
    }
  ]
})
